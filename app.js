var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cors = require('cors');
const conn = require('./dbConfig/mongo');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./api/swagger/swagger.json');

var index = require('./routes/network');
var users = require('./routes/apis/users');
var login = require('./routes/apis/login');
var register = require('./routes/apis/signup');
var channels = require('./routes/network/channels');
var chaincode = require('./routes/network/chaincode');
var peer = require('./routes/network/peers');
var query = require('./routes/network/querys');
var regVaccine = require('./routes/apis/storage');
var iot = require('./routes/apis/iot');


var app = express();

// SET THE PORT
var port = process.env.PORT || 8900;
app.listen(port);
console.log('Magic happens on port ' + port);


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');


// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Allow COR
app.use(cors())

//Routes
app.use('/', index);
app.use('/api/getAllUsers', users);
app.use('/api/login', login);
app.use('/api/register', register);
app.use('/api/store', regVaccine);
app.use('/api/iot', iot);
app.use('/channels', channels);
app.use('/chaincode', chaincode);
app.use('/peer', peer);
app.use('/query', query);
// swagger-ui
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');var cors = require('cors')
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
