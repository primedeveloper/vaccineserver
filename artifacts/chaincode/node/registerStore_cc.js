const shim = require('fabric-shim');

const Chaincode = class {
    async Init(stub) {
        console.info('========= ChainCode RegisterStore Init =========');
        let ret = stub.getFunctionAndParameters();
        console.info(ret);
        let args = ret.params;

        /** initialise only if 5 parameters passed. */
        // if (args.length != 9) {
        //     return shim.error(Buffer.from('Incorrect number of arguments. Expecting 9'));
        // }

        let level = args[1];
        let storename = args[3];
        let capacity = args[5];
        let location = args[7];
        let description = args[9];


        try {
            await stub.putState(level, Buffer.from(level));
            await stub.putState(storename, Buffer.from(storename));
            await stub.putState(capacity, Buffer.from(capacity));
            await stub.putState(location, Buffer.from(location));
            await stub.putState(description, Buffer.from(description));
            return shim.success(Buffer.from('Successfully added to block')); //txid
        } catch (err) {
            return shim.error(err);
        }
    }

    async Invoke(stub) {
        let ret = stub.getFunctionAndParameters();
        console.info(ret);
        let method = this[ret.fcn];
        if (!method) {
            console.error('no method of name:' + ret.fcn + ' found');
            return shim.error('no method of name:' + ret.fcn + ' found');
        }

        console.info('\nCalling method : ' + ret.fcn);
        try {
            let payload = await method(stub, ret.params);
            return shim.success(payload);
        } catch (err) {
            console.log(err);
            return shim.error(err);
        }
    }

    async query(stub, args) {

    }
};

shim.start(new Chaincode());
