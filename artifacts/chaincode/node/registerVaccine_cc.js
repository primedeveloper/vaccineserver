const shim = require('fabric-shim');

const Chaincode = class {
    async Init(stub) {
        console.info('========= ChainCode RegisterVaccine Init =========');
        let ret = stub.getFunctionAndParameters();
        console.info(ret);
        let args = ret.params;

        /** initialise only if 4 parameters passed. */
        if (args.length != 13) {
            return shim.error(Buffer.from('Incorrect number of arguments. Expecting 10'));
        }

        let antigenType = args[1];
        let location = args[3];
        let batchID = args[5];
        let beaconID = args[7];
        let owner = args[9];
        let temperature = args[11];
        let status = args[13];


        try {
            await stub.putState(antigenType, Buffer.from(Aval));
            await stub.putState(location, Buffer.from(Bval));
            await stub.putState(batchID, Buffer.from(Bval));
            await stub.putState(beaconID, Buffer.from(Bval));
            await stub.putState(owner, Buffer.from(Bval));
            await stub.putState(temperature, Buffer.from(Bval));
            await stub.putState(status, Buffer.from(Bval));
            return shim.success(Buffer.from('Successfully added to block')); //txid
        } catch (err) {
            return shim.error(err);
        }
    }

    async Invoke(stub) {
        let ret = stub.getFunctionAndParameters();
        console.info(ret);
        let method = this[ret.fcn];
        if (!method) {
            console.error('no method of name:' + ret.fcn + ' found');
            return shim.error('no method of name:' + ret.fcn + ' found');
        }

        console.info('\nCalling method : ' + ret.fcn);
        try {
            let payload = await method(stub, ret.params);
            return shim.success(payload);
        } catch (err) {
            console.log(err);
            return shim.error(err);
        }
    }

    async store(stub,args) {

    }

    async query(stub, args) {
        let data = {
             antigenType:  await stub.getState(antigenType),
             location:  await stub.getState(location),
             batchID:  await stub.getState(batchID),
             beaconID:  await stub.getState(beaconID),
             owner:  await stub.getState(owner),
             temperature:  await stub.getState(temperature),
             status:  await stub.getState(status),
        }

        return shim.success(data)
    }
};

shim.start(new Chaincode());
