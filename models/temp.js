/**
 * schema
 */
var mongoose = require('mongoose');

//registerTemp Schema
var tempSchema = mongoose.Schema({

    tempValue:{
        type:String,
    },
   
    dateCreated: {
        type: Date,
        default: Date.now
    }

});

var Temp = mongoose.model('Temp',tempSchema,'temp');
module.exports = Temp;
