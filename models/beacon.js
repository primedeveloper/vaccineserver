/**
 * Created by user on 4/4/18.
 */
var mongoose = require('mongoose');

//registerbeacon Schema
var registerbeaconSchema = mongoose.Schema({
    beaconID:{
        type:String,
        required:true,
        unique:true,
    },
    beaconSerial:{
        type:String,
        required:true,
        unique:true,
    },
    beaconname:{
        type:String,
        required:true,
        unique:true,
    },
    manufacturer:{
        type:String,
        required:true
    },
    dateCreated: {
        type: Date,
        default: Date.now
    }

});

var Beacon = mongoose.model('Beacon',registerbeaconSchema,'beacon');
module.exports = Beacon;
