/**
 * Created by user on 4/4/18.
 */
var mongoose = require('mongoose');

//registerbeacon Schema
var warehouseSchema = mongoose.Schema({
    warehouseID:{
        type:String,
        required:true,
        unique:true,
    },
    storename:{
        type:String,
        required:true,
        unique:true,
    },
    department:{
        type:String,
        required:true,
        unique:true,
    },
    capacity:{
        type:String,
        required:true
    },
    location:{
        type:String,
        required:true
    },
    dateCreated: {
        type: Date,
        default: Date.now
    }

});

var Warehouse = mongoose.model('Warehouse',warehouseSchema,'warehouse');
module.exports = Warehouse;
