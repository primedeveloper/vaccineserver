/**
 * Created by user on 4/4/18.
 */
var mongoose = require('mongoose');

//registerdriver Schema
var registerdriverSchema = mongoose.Schema({
    driverID:{
        type:String,
        required:true,
        unique:true,
    },
    driverLicenceID:{
        type:String,
        required:true,
        unique:true,
    },
    nationalID:{
        type:String,
        required:true,
        unique:true,
    },
    company:{
        type:String,
        required:true
    },
    fname:{
        type:String,
        required:true
    },
    lname:{
        type:String,
        required:true
    },
    dateCreated: {
        type: Date,
        default: Date.now
    }

});

var Driver = mongoose.model('Driver',registerdriverSchema,'driver');
module.exports = Driver;
