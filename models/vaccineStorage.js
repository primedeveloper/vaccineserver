/**
 * Created by user on 4/4/18.
 */
var mongoose = require('mongoose');

//registervaccine Schema
var registervaccineSchema = mongoose.Schema({
    antigenType:{
        type:String,
        required:true
    },
    location:{
        type:String,
        required:true
    },
    batchID:{
        type:String,
        unique:true,
        required:true
    },
    expirydate:{
        type:String,
        required:true
    },
    manufacturer:{
        type:String,
        required:true
    },
    beaconID:{
        type:String,
        required:true
    },
    description:{
        type:String,
        required:false
    },
    temperature: {
        type:Number,
        required:true
    },
    state:{
        type:String,
        required:true
    },
    condition: {
        type:Boolean,
        default:true
    },
    dateCreated: {
        type: Date,
        default: Date.now
    }

});

var RegisterVaccine = mongoose.model('RegisterVaccine',registervaccineSchema,'registervaccine');
module.exports = RegisterVaccine;
