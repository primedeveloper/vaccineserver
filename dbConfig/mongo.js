var mongoose = require('mongoose');
var log4js = require('log4js');
var logger = log4js.getLogger("MONGODBCONNECTION");
logger.level = 'debug';

// mongodb://root:password@34@ds119024.mlab.com:19024
function dbConnection () {
    mongoose.connect('mongodb://127.0.0.1:27017/fabric',{ useNewUrlParser: true },function(err) {
        if (err) {
            logger.error('MongoDB connection error: ' + err);
            process.exit(1);
        }else{
            logger.info("Connected Successfully")
        }
    });

    // Get Mongoose to use the global promise library
    mongoose.Promise = global.Promise;

    var conn = mongoose.connection;

    conn.on('error',function(){
        logger.error(console, 'connection error:')
    });
    conn.once('open', function() {
        logger.info("Mongodb is up and Running");
    });

};

module.export = dbConnection ()
