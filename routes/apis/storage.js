var express = require('express');
var router = express.Router();
var log4js = require('log4js');
var logger = log4js.getLogger('RegisterVaccineAPI');
var instantiate = require('../../scripts/instantiate-chaincode');
var invoke = require('../../scripts/invoke-transaction');
const auth = require('../../middleware/auth');
var helper = require('../../scripts/helpers')
var Vaccine = require('../../models/vaccineStorage');
var Beacon= require('../../models/beacon');
var Driver= require('../../models/driver');
var Warehouse= require('../../models/warehouse');

/**
 * RegisterVaccine
 * api/iot/vaccine/register
 * */
router.post('/vaccine/register', async function(req, res, next) {

    var params = {
        'orderID': helper.setTranstypeID('REGISTER_VACCINE'),
        'antigenType':  req.body.antigenType,
        'location': req.body.location,
        'batchID': req.body.batchID,
        'expirydate': req.body.expirydate,
        'manufacturer': req.body.manufacturer,
        'beaconID': req.body.beaconID,
        'description': req.body.description,
        'temperature': req.body.temperature,
        'state': req.body.state,
        'peers': req.body.peers,
        'chaincodeName': req.body.chaincodeName,
        'chaincodeVersion': req.body.chaincodeVersion,
        'channelName': req.body.channelName,
        'chaincodeType': req.body.chaincodeType,
        'username': req.body.username,
        'orgname': req.body.orgname
    };

    console.log(JSON.stringify(params));

    Vaccine.findOne({'batchID': params.batchID}, function (error, results) {
        if(error){
            logger.error(error);
        }else{
            if(!results){
                /*db query*/
                var newregistervaccine = new Vaccine();
                newregistervaccine.antigenType = params.antigenType;
                newregistervaccine.location = params.location;
                newregistervaccine.batchID = params.batchID;
                newregistervaccine.expirydate = params.expirydate;
                newregistervaccine.manufacturer = params.manufacturer;
                newregistervaccine.beaconID = params.beaconID;
                newregistervaccine.description = params.description;
                newregistervaccine.temperature = params.temperature;
                newregistervaccine.state = params.state;

                Vaccine.save(function (error, savedUser){
                    if (error){
                        logger.error(error);
                    }else {
                        logger.debug(savedUser);
                        return res.send(helper.getSuccessMessage(savedUser));
                    }
                });

            }
        }
    });

    /** STORE DATA IN DB FIRST*/
    // storeVaccineDetails(params).then(async (data) =>{
    //     logger.debug("==== IS VACCINED %s =========", data);
    //     if(data.success){
    //         return response.send(helper.getSuccessMessage(data));
    //     }
    //     return response.send(helper.getErrorMessage(error));
    // })
});

/**
 * GET RegisterVaccine
 * api/iot/vaccine/getall
 * */
router.get('/vaccine/getall', function (req,res,next){
    logger.debug("\n\n============== FETCH ALL REGISTERED VACCINES ================");

    Vaccine.find({}, (error,data)=>{
        if(error){
            return res.json(helper.getErrorMessage(error));
        }
        return res.json(helper.getSuccessMessage(data))
    })
});

/**
 * RegisterBeacon
 * api/iot/beacon/register
 * */
router.post('/beacon/register', async function(req, res, next) {

    var params = {
        'beaconID': helper.setTranstypeID('REGISTER_BEACON'),
        'manufacturer': req.body.manufacturer,
        'beaconSerial': req.body.beaconID,
        'beaconname': req.body.beaconname
    };

    console.log(JSON.stringify(params));
    Beacon.findOne({'beaconID': params.beaconID}, function (error, results) {
        if(error){
            logger.error(error);
            return res.send(helper.getErrorMessage(error))
        }else{
            if(!results){
                /*db query*/
                var newregisterbeacon = new Beacon();
                newregisterbeacon.manufacturer = params.manufacturer;
                newregisterbeacon.beaconID = params.beaconID;
                newregisterbeacon.beaconSerial = params.beaconSerial;
                newregisterbeacon.beaconname = params.beaconname;

                newregisterbeacon.save(function (error, savedUser){
                    if (error){
                        logger.error(error);
                    }else {
                        logger.debug(savedUser);
                        return res.send(helper.getSuccessMessage(savedUser));
                    }
                });

            }
        }
    });

    /** STORE BEACONE IN BLOCK*/
    // storeVaccineDetails(params).then(async (data) =>{
    //     logger.debug("==== IS VACCINED %s =========", data);
    //     if(data.success){
    //         return response.send(helper.getSuccessMessage(data));
    //     }
    //     return response.send(helper.getErrorMessage(error));
    // })
});

/**
 * GET RegisterBeacon
 * api/iot/beacon/getall
 * */
router.get('/beacon/getall', function (req,res,next){
    logger.debug("\n\n============== FETCH ALL REGISTERED VACCINES ================");

    Beacon.find({}, (error,data)=>{
        if(error){
            return res.json(helper.getErrorMessage(error));
        }
        return res.json(helper.getSuccessMessage(data))
    })
});

/**
 * RegisterDriver
 * api/iot/driver/register
 * */
router.post('/driver/register', async function(req, res, next) {

    var params = {
        'driverID': helper.setTranstypeID('REGISTER_DRIVER'),
        'driverLicenceID': req.body.driverID,
        'nationalID': req.body.nationalID,
        'company': req.body.company,
        'fname': req.body.fname,
        'lname': req.body.lname,
    };

    console.log(JSON.stringify(params));
    RegisterDriver.findOne({'driverID': params.driverID}, function (error, results) {
        if(error){
            logger.error(error);
            return res.send(helper.getErrorMessage(error))
        }else{
            if(!results){
                /*db query*/
                var newregisterDriver = new Driver();
                newregisterDriver.driverID = params.driverID;
                newregisterDriver.driverLicenceID = params.driverLicenceID;
                newregisterDriver.nationalID = params.nationalID;
                newregisterDriver.company = params.company;
                newregisterDriver.fname = params.fname;
                newregisterDriver.lname = params.lname;

                newregisterDriver.save(function (error, savedUser){
                    if (error){
                        logger.error(error);
                    }else {
                        logger.debug(savedUser);
                        return res.send(helper.getSuccessMessage(savedUser));
                    }
                });

            }
        }
    });

    /** STORE DRIVER IN BLOCK*/
    // storeVaccineDetails(params).then(async (data) =>{
    //     logger.debug("==== IS VACCINED %s =========", data);
    //     if(data.success){
    //         return response.send(helper.getSuccessMessage(data));
    //     }
    //     return response.send(helper.getErrorMessage(error));
    // })
});

/**
 * GET RegisterDriver
 * api/iot/driver/getall
 * */
router.get('/driver/getall', function (req,res,next){
    logger.debug("\n\n============== FETCH ALL REGISTERED VACCINES ================");

    Driver.find({}, (error,data)=>{
        if(error){
            return res.json(helper.getErrorMessage(error));
        }
        return res.json(helper.getSuccessMessage(data))
    })
});

/**
 * RegisterWarehouse
 * api/iot/warehouse/register
 * */
router.post('/warehouse/register', async function(req, res, next) {

    var params = {
        'warehouseID': helper.setTranstypeID('REGISTER_WAREHOUSE'),
        'department': req.body.department,
        'storename': req.body.storename,
        'capacity': req.body.capacity,
        'location': req.body.location
    };

    console.log(JSON.stringify(params));
    Warehouse.findOne({'storename': params.storename}, function (error, results) {
        if(error){
            logger.error(error);
            return res.send(helper.getErrorMessage(error))
        }else{
            if(!results){
                /*db query*/
                var newwarehouse= new Warehouse();
                newwarehouse.warehouseID = params.warehouseID;
                newwarehouse.department = params.department;
                newwarehouse.storename = params.storename;
                newwarehouse.capacity = params.capacity;
                newwarehouse.location = params.location;

                newwarehouse.save(function (error, savedUser){
                    if (error){
                        logger.error(error);
                    }else {
                        logger.debug(savedUser);
                        return res.send(helper.getSuccessMessage(savedUser));
                    }
                });

            }
        }
    });

    /** STORE DRIVER IN BLOCK*/
    // storeVaccineDetails(params).then(async (data) =>{
    //     logger.debug("==== IS VACCINED %s =========", data);
    //     if(data.success){
    //         return response.send(helper.getSuccessMessage(data));
    //     }
    //     return response.send(helper.getErrorMessage(error));
    // })
});

/**
 * GET RegisterWarehouse
 * api/iot/warehouse/getall
 * */
router.get('/warehouse/getall', function (req,res,next){
    logger.debug("\n\n============== FETCH ALL REGISTERED VACCINES ================");

    Warehouse.find({}, (error,data)=>{
        if(error){
            return res.json(helper.getErrorMessage(error));
        }
        return res.json(helper.getSuccessMessage(data))
    })
});

const storeVaccineDetails =  (params) =>{
    return new Promise( async (reject,resolve) =>{

        var peers = params.peers;
        var chaincodeName = params.chaincodeName;
        var chaincodeVersion = params.chaincodeVersion;
        var channelName = params.channelName;
        var chaincodeType = params.chaincodeType;
        var fcn = "Init";
        var username = params.username;
        var orgname = params.orgname;

        let args = [
            'antigenType',  params.antigenType,
            'location', params.location,
            'batchID', params.batchID,
            'expirydate', params.expirydate,
            'manufacturer', params.manufacturer,
            'beaconID', params.beaconID,
            'description', params.description,
            'temperature', params.temperature,
            'state', params.state
        ];

        logger.debug('username  : ' + username);
        logger.debug('orgname  : ' + orgname);
        logger.debug('peers  : ' + peers);
        logger.debug('channelName  : ' + channelName);
        logger.debug('chaincodeName : ' + chaincodeName);
        logger.debug('chaincodeVersion  : ' + chaincodeVersion);
        logger.debug('chaincodeType  : ' + chaincodeType);
        logger.debug('fcn  : ' + fcn);
        logger.debug('args  : ' + args);

        if (!chaincodeName) {
            res.json(helper.getErrorMessage('\'chaincodeName\''));
            return;
        }
        if (!chaincodeVersion) {
            res.json(helper.getErrorMessage('\'chaincodeVersion\''));
            return;
        }
        if (!channelName) {
            res.json(helper.getErrorMessage('\'channelName\''));
            return;
        }
        if (!chaincodeType) {
            res.json(helper.getErrorMessage('\'chaincodeType\''));
            return;
        }
        if (!args) {
            res.json(helper.getErrorMessage('\'args\''));
            return;
        }

        // let message = await instantiate.instantiateChaincode(peers, channelName, chaincodeName, chaincodeVersion, fcn ,chaincodeType, args, username, orgname);
        let message = await invoke.invokeChaincode(peers, channelName, chaincodeName, chaincodeVersion, fcn ,chaincodeType, args, username, orgname);
        resolve(helper.getSuccessMessage(message));
  });
}

module.exports = router;
