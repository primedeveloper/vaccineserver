var express = require('express');
var router = express.Router();
var log4js = require('log4js');
var logger = log4js.getLogger('RegisterVaccineAPI');
var instantiate = require('../../scripts/instantiate-chaincode');
const auth = require('../../middleware/auth');
var helper = require('../../scripts/helpers')

/** PurchaseOrder */
router.post('/', auth, async function(req, res, next) {

    let antigenType= req.body.antigenType
    let location= req.body.location
    let batchID = req.body.batchID
    let beaconID = req.body.beaconID
    let owner = req.body.owner
    let temperature= req.body.temperature
    var peers = req.body.peers;
    var chaincodeName = req.body.chaincodeName;
    var chaincodeVersion = req.body.chaincodeVersion;
    var channelName = req.body.channelName;
    var chaincodeType = req.body.chaincodeType;
    var fcn = "Init";
    var username = req.body.username;
    var orgname = req.body.orgname;
    let args = [
        "antigenType",antigenType,
        "location",location,
        "batchID",batchID,
        "beaconID",beaconID,
        "owner",owner,
        "temperature",temperature,
    ];

    logger.debug('username  : ' + username);
    logger.debug('orgname  : ' + orgname);
    logger.debug('peers  : ' + peers);
    logger.debug('channelName  : ' + channelName);
    logger.debug('chaincodeName : ' + chaincodeName);
    logger.debug('chaincodeVersion  : ' + chaincodeVersion);
    logger.debug('chaincodeType  : ' + chaincodeType);
    logger.debug('fcn  : ' + fcn);
    logger.debug('args  : ' + args);

    if (!chaincodeName) {
        res.json(helper.getErrorMessage('\'chaincodeName\''));
        return;
    }
    if (!chaincodeVersion) {
        res.json(helper.getErrorMessage('\'chaincodeVersion\''));
        return;
    }
    if (!channelName) {
        res.json(helper.getErrorMessage('\'channelName\''));
        return;
    }
    if (!chaincodeType) {
        res.json(helper.getErrorMessage('\'chaincodeType\''));
        return;
    }
    if (!args) {
        res.json(helper.getErrorMessage('\'args\''));
        return;
    }

    let message = await instantiate.instantiateChaincode(peers, channelName, chaincodeName, chaincodeVersion, fcn ,chaincodeType, args, username, orgname);
    res.send(message);

});

module.exports = router;
