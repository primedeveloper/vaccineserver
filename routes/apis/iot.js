var express = require('express');
var router = express.Router();
var iotController = require('../controllers/iotController');

var log4js = require('log4js');
var logger = log4js.getLogger('RegisterStoreController');
var instantiate = require('../../scripts/instantiate-chaincode');
var invoke = require('../../scripts/invoke-transaction');
const auth = require('../../middleware/auth');
var helper = require('../../scripts/helpers');

/** Register STORE */

/**
 * /api/iot
 */
router.get('/',iotController.meshlium);

module.exports = router;
