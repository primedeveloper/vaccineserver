var express = require('express');
var router = express.Router();
var log4js = require('log4js');
var logger = log4js.getLogger("RegisterUserAPI");
logger.level = 'debug';
var User = require("../../models/users")
var helper = require('../../scripts/helpers');
const auth = require('../../middleware/auth');


/** CREATES NEW USER */
router.post('/user', async function(req, res, next) {

  var param = {
    'email':req.body.email,
    'username':req.body.username,
    'password':req.body.password,
    'phone':req.body.phone,
    'level':req.body.level,
    'role':req.body.role
};

console.log(JSON.stringify(param))

User.findOne({'email': param.email}, function (error, results) {
        if(error){
            res.send(helper.getErrorMessage(error));
        }else{
             if(!results){
                 /*db query*/
                 var newuser = new User();
                 newuser.email = param.email;
                 newuser.username = param.username;
                 newuser.password = newuser.generateHash(param.password);
                 newuser.phone = param.phone;
                 newuser.level = param.level;
                 newuser.roles = param.role;

                 newuser.save(function (error, savedUser){
                     if (error){
                         res.send(helper.getErrorMessage(error));
                     }else {
                         res.send(helper.getSuccessMessage(savedUser));
                     }
                 });

             }else{
                 res.send(helper.getErrorMessage("user exists"));
             }

        }
  });

});

router.post('/network/user/:id', async function(req, res, next) {

    var param = {};

    User.find({"email":req.params.id}, async (error,data)=>{
        if(error){
            return res.json(helper.getErrorMessage(error));
        }
        logger.info("DATA FETCHED ",JSON.stringify(data));

        param = {
            'email':data[0].email,
            'username':data[0].username,
            'password':data[0].password,
            'phone':data[0].phone,
            'level': data[0].level,
            'role': data[0].roles,
            'org':"org1"
        };

        var ca_response = await enrollUser(param);
        res.json(ca_response)
    })
});

const enrollUser = (param) =>{

    logger.info(JSON.stringify(param))

    /**Check if all params have been passed*/
    if(param.email =="" || param.password == null || param.phone =="" || param.username == ""){
        res.json(helper.getErrorMessage("All the fields are required"));
        return;
    }

    /**Call FabricClient to register User**/
    helper.getRegisteredUser(param.username, param.org, true).then(function(response) {
        try{
            logger.debug('-- returned from registering the username %s for organization %s',param.username,param.org);
            logger.debug('-- returned responce  %s ',JSON.stringify(response));

            if (response && typeof response !== 'string') {
                logger.debug('Successfully registered the username %s for organization %s',param.username,param.org);
                return helper.getSuccessMessage(response);
            } else {
                logger.debug('Failed to register the username %s for organization %s with::%s',param.username,param.org,response);
                return helper.getErrorMessage(response);
            }
        }catch (e) {
            logger.error(e)
        }
    });
}

module.exports = router;
