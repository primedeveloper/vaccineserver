var express = require('express');
var router = express.Router();
const auth = require('../../middleware/auth')


// Update anchor peers
router.post('/channels/:channelName/anchorpeers',auth, async function(req, res) {
    logger.debug('==================== UPDATE ANCHOR PEERS ==================');
    var channelName = req.params.channelName;
    var configUpdatePath = req.body.configUpdatePath;
    logger.debug('Channel name : ' + channelName);
    logger.debug('configUpdatePath : ' + configUpdatePath);
    if (!channelName) {
        res.json(getErrorMessage('\'channelName\''));
        return;
    }
    if (!configUpdatePath) {
        res.json(getErrorMessage('\'configUpdatePath\''));
        return;
    }

    let message = await updateAnchorPeers.updateAnchorPeers(channelName, configUpdatePath, req.username, req.orgname);
    res.send(message);
});


module.exports = router;
