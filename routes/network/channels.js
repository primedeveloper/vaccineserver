var express = require('express');
var router = express.Router();
var log4js = require('log4js');
var logger = log4js.getLogger('CreateChannelApi');
var createChannel = require('../../scripts/createChannel');
var join = require('../../scripts/join-channel');
var helper = require('../../scripts/helpers')
const auth = require('../../middleware/auth')

/** CREATE CHANNELS*/
router.post('/create',function(req, res, next) {
    logger.debug('<<<<<<<<<<<<<<<<< C R E A T E  C H A N N E L >>>>>>>>>>>>>>>>>');
    logger.debug('End point : /channels');

    var channelName = req.body.channelName;
    var channelConfigPath = req.body.channelConfigPath;
    var username = req.body.username;
    var orgname = req.body.orgname;

    logger.debug('Channel name : ' + channelName);
    logger.debug('channelConfigPath : ' + channelConfigPath); //../artifacts/channel/mychannel.tx
    if (!channelName) {
        res.json(helper.getErrorMessage('\'channelName\''));
        return;
    }
    if (!channelConfigPath) {
        res.json(helper.getErrorMessage('\'channelConfigPath\''));
        return;
    }

    createChannel.createChannel(channelName, channelConfigPath, username, orgname).then(message =>{
        logger.debug("\n\n<<<<<<<<<<<<<<<<<<<< \n Response From CREATE CHANNEL  \n\n"+JSON.stringify(message)+ "\n\n >>>>>>>>>>>>>>>>>>>>>")
        res.json(helper.getSuccessMessage(message));
    });

});

/** JOIN CHANNELS*/
router.post('/joinchannels', async function(req, res) {
    logger.debug('<<<<<<<<<<<<<<<<< J O I N  C H A N N E L >>>>>>>>>>>>>>>>>');
    var channelName = req.body.channelName;
    var peers = req.body.peers;
    var username = req.body.username;
    var orgname = req.body.orgname;

    logger.debug('channelName : ' + channelName);
    logger.debug('peers : ' + peers);
    logger.debug('username :' + username);
    logger.debug('orgname:' + orgname);

    if (!channelName) {
        res.json(getErrorMessage('\'channelName\''));
        return;
    }
    if (!peers || peers.length == 0) {
        res.json(getErrorMessage('\'peers\''));
        return;
    }

    let message =  await join.joinChannel(channelName, peers, username, orgname);
    res.send(helper.getSuccessMessage(message));
});


module.exports = router;
