var log4js = require('log4js');
var logger = log4js.getLogger("UsersAPI");
var User = require("../../models/users")
logger.level = 'debug';
var helper = require('../../scripts/helpers');


const user =  (req, res, next) => {
    logger.debug("\n\n============== FETCH ALL USERS ================");
    logger.debug("HEADERS "+JSON.stringify(req.headers));

    User.find({}, (error,data)=>{
        if(error){
            return res.json(helper.getErrorMessage(error));
        }
        return res.json(helper.getSuccessMessage(data))
    })
}

module.exports = user;
