var log4js = require('log4js');
var logger = log4js.getLogger('IOTController');
var Temp = require('../../models/temp');
var helper = require('../../scripts/helpers');

const meshlium = async (req,res, next)=> {
    Temp.find({},(err, saved) => {
        if(!err){
            return res.json(helper.getSuccessMessage(saved));
        }
    });
};

exports.meshlium = meshlium;
