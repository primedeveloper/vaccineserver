/**
 * JOE
 */
'use strict';
var log4js = require('log4js');
var logger = log4js.getLogger('Helper');
logger.level = 'debug';

var path = require('path');
var util = require('util');
var hfc = require('fabric-client');
hfc.setLogger(logger);

async function getClientForOrg (userorg, username) {
    logger.debug('\n\n ================ \n getClientForOrg - ****** START %s %s \n ', userorg, username +" \n\n ==============\n")

    // get a fabric client loaded with a connection profile for this org
    let config = './artifacts/network-config.yaml';

    // build a client context and load it with a connection profile
    // lets only load the network settings and save the client for later
    var client = hfc.loadFromConfig(config);
    let department = userorg;


    switch (department) {
        case "org1":
            client.loadFromConfig('./artifacts/org1.yaml');
            await client.initCredentialStores().then(()=> {
                logger.info("state store created and assigned to %s ",username)
            });
            logger.debug('getClientForOrg - ****** CONNECTION SUCCESSFUL %s %s \n\n', userorg, username)
            return client;
        default:
            return new Error(util.format('Department was not found :', department));
    }

}

var getRegisteredUser = async function(username, userOrg, isJson) {
    try {
        var client = await getClientForOrg(userOrg,username);
        console.log('Successfully initialized the credential stores ');

        // client can now act as an agent for organization Org1
        // first check to see if the user is already enrolled
        var user = await client.getUserContext(username, true);

        if (user && user.isEnrolled) {
            logger.info('User %s loaded member from persistence ',username);
        } else {
            // user was not enrolled, so we will need an admin user object to register
            logger.info('User %s was not enrolled, so we will need an admin user object to register',username);

            var admins = hfc.getConfigSetting('admin');

            /***Check if admin exists*/
            if(admins){
                logger.info('Admin %s exists ');
            }else {
                var error = "Admin doesnt Exist, Please enroll one"
                logger.warn('Failed to get registered user: %s with error: %s', username, error.toString());
                logger.warn('Try to register a new Admin');
            }


            /**Register ADMIN*/
            let adminUserObj = await client.setUserContext({username: 'admin', password: 'adminpw'});


            /**Register USER*/
            if(adminUserObj != null){
                let caClient = client.getCertificateAuthority();
                let secret = await caClient.register({
                    enrollmentID: username,
                    affiliation: userOrg,
                }, adminUserObj);
                logger.debug('Successfully got the secret for user %s',username);
                user = await client.setUserContext({username:username, password:secret});
                logger.debug('Successfully enrolled username %s  and setUserContext on the client object 🤪🤪🤪🤪', user);
            }

            if(user && user.isEnrolled) {
                if (isJson && isJson === true) {
                    var response = {
                        success: true,
                        secret: user._enrollmentSecret,
                        message: username + ' enrolled Successfully',
                    };
                    return response;
                }
            } else {
                throw new Error('User was not enrolled ');
            }
        }


    } catch(error) {
        logger.error('Failed to get registered user: %s with error: %s', username, error.toString());
        return 'failed '+error.toString();
    }

};

var setupChaincodeDeploy = function() {
    process.env.GOPATH = path.join(__dirname, hfc.getConfigSetting('CC_SRC_PATH'));
};

var getLogger = function(moduleName) {
    var logger = log4js.getLogger(moduleName);
    logger.level = 'debug';
    return logger;
};

var getErrorMessage = function (field) {
    var response = {
        success: false,
        message: field + ' field is missing or Invalid in the request'
    };
    return response;
};

var getSuccessMessage = function (field) {
    var response = {
        success: true,
        message: field
    };
    return response;
};

var setTranstypeID = function (transtype){

    switch (transtype) {
        case 'PROCUREMENT_ORDER':
            return 'PO'+generateTransID(transtype);
        case 'VACCINE_ORDER':
            return 'VO'+generateTransID(transtype);
        case 'TRANSIT_ORDER':
            return 'TO'+generateTransID(transtype);
        case 'REGISTER_VACCINE':
            return 'RV'+generateTransID(transtype);
        case 'REGISTER_BEACON':
            return 'RV'+generateTransID(transtype);
        case 'REGISTER_DRIVER':
            return 'RV'+generateTransID(transtype);
        case 'REGISTER_WAREHOUSE':
            return 'RV'+generateTransID(transtype);
        default:
            return'INVALID'+generateTransID(transtype);
    }
};

var generateTransID = function (transtype){
    let length = 8;
    let timestamp = +new Date;

    var _getRandomInt = function (min, max) {
        return Math.floor(Math.random() * ( max - min + 1 )) + min;
    }

    var ts = timestamp.toString();
    var parts = ts.split("").reverse();
    var id = "";

    for (var i = 0; i < length; ++i) {
        var index = _getRandomInt(0, parts.length - 1);
        id += parts[index];
    }

    return id;
};

exports.getClientForOrg = getClientForOrg;
exports.getLogger = getLogger;
exports.setupChaincodeDeploy = setupChaincodeDeploy;
exports.getRegisteredUser = getRegisteredUser;
exports.getErrorMessage = getErrorMessage;
exports.getSuccessMessage = getSuccessMessage;
exports.setTranstypeID = setTranstypeID;
