/**
 * joe
 */

var util = require('util');
var fs = require('fs');
var path = require('path');

var helper = require('./helpers.js');
var logger = helper.getLogger('Create-Channel');
var hfc = require('fabric-client');
var departs = require('./enum').Departments;

/** Attempt to send a request to the orderer with the sendTransaction method */
var createChannel = async function(channelName, channelConfigPath, username, orgName) {
    logger.debug('\n====== Creating Channel \'' + channelName + '\' ======\n');
    logger.debug('====== Channel Config ' + channelConfigPath + ' ======');
    logger.debug('====== Username ' + username + ' ======');
    logger.debug('====== Department ' + orgName + '======');

        //determine valid org
        switch (orgName) {
            case 'org1':
                var conn = await connection(channelName,channelConfigPath,username,orgName)
                return conn;
            default:
                logger.error("error ",orgName)
                throw new Error(util.format('Department was not found :', orgName));
        }

}

async function connection(channelName, channelConfigPath, username, orgName){
       try {
        logger.debug("tobe passed to helper ",{channelName,channelConfigPath,username,orgName});

        /**first setup the client for this org*/
        var client = await helper.getClientForOrg(orgName,username);
        logger.debug('Successfully got the fabric client for the organization "%s"', orgName);

       // var user = await client.getUserContext(username, true);
       // var admins = hfc.getConfigSetting('admins');
       //
       // if(!user && !admins ){
       //     let response = {
       //         success: "Failed",
       //         message: 'Username \'' + username + '\' doesnt Exist'
       //     };
       //     return response;
       // }
       //
       // logger.debug('User %s exist loaded from persistence ', username)

        /**read in the envelope for the channel config raw bytes*/
        var envelope = fs.readFileSync(channelConfigPath);
        /**extract the channel config bytes from the envelope to be signed*/
        var channelConfig = client.extractChannelConfig(envelope);

        logger.debug("channelConfig "+channelConfig)

        /** Acting as a client in the given organization provided with "orgName" param
        // sign the channel config bytes as "endorsement", this is required by
        // the orderer's channel creation policy
        // this will use the admin identity assigned to the client when the connection profile was loaded */
        let signature = client.signChannelConfig(channelConfig);

       // have the SDK generate a transaction id
       let tx_id = client.newTransactionID(true); // get an admin based transactionID

       let request = {
           config: channelConfig, //the binary config
           signatures : [signature], // the collected signatures
           name : channelName, // the channel name
           txId  : tx_id //the generated transaction id
       };

        logger.debug("Create a Channel request "+request)

        /** send to orderer*/
       var response =  await client.createChannel(request);
        if (response && response.status === 'SUCCESS') {
            logger.debug('Successfully created the channel.');
            let response = {
                success: true,
                message: 'Channel \'' + channelName + '\' created Successfully'
            };
            return response;
        } else {
            let res = {
                success: false,
                message: response
            };
            logger.error('response ::%j ', response);
            logger.error('\n!!!!!!!!! Failed to create the channel \'' + channelName +
                '\' !!!!!!!!!\n\n');
            // throw new Error('Failed to create the channel \'' + channelName + '\'');
            // logger.debug("channel res "+JSON.stringify(res))
            return res;
        }


    } catch (err) {
        logger.error('Failed to initialize the channel: ' + err.stack ? err.stack :	err);
        throw new Error('Failed to initialize the channel: ' + err.toString());
    }
};


exports.createChannel = createChannel;
