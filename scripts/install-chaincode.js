/**
 *  limitations under the License.
 */


'use strict';
var path = require('path');
var fs = require('fs');
var util = require('util');
var helper = require('./helpers.js');
var logger = helper.getLogger('install-chaincode');
var tx_id = null;
var hfc = require('fabric-client');
hfc.setLogger(logger);

var installChaincode = async function(peers, chaincodeName,
	chaincodeVersion, chaincodeType, username, org_name) {
	logger.debug('\n\n============\n\n Install chaincode on organizations \n\n============\n');

	// some other settings the application might need to know
	hfc.addConfigFile('./networkConfig/config.json');
	helper.setupChaincodeDeploy();

	var chaincodePath;

	switch (chaincodeName) {
		case 'registerVaccine':
			chaincodePath = path.join(__dirname, '../artifacts/chaincode/node');
			break;
		case 'registerBecon':
			chaincodePath = path.join(__dirname, '../artifacts/chaincode/node');
			break;
		case 'registerStore':
			chaincodePath = path.join(__dirname, '../artifacts/chaincode/node');
			break;
		default:
			let message = util.format('Failed to install Invalid Chaincode');
			logger.error(message);
			let response = {
				success: false,
				message: message
			};
			return response;
	}

	console.log("PATH "+chaincodePath);

	let error_message = null;
	try {

		logger.info('Calling peers in organization "%s" to join the channel', org_name);

		/** first setup the client for this org */
		var client = await helper.getClientForOrg(org_name, username);
		logger.debug('Successfully got the fabric client for the organization "%s"', org_name);

		/** get an admin transactionID */
		tx_id = client.newTransactionID(true);
		var request = {
			targets: peers,
			chaincodePath: chaincodePath,
			chaincodeId: chaincodeName,
			chaincodeVersion: chaincodeVersion,
			chaincodeType: chaincodeType
		};

		logger.debug("\n\n =============\n Install ChainCode Request \n "+ JSON.stringify(request)+" \n================\n\n")
		let results = await client.installChaincode(request);
		// logger.debug("response ::%j",results)
		/**
		 * the returned object has both the endorsement results
		 * and the actual proposal, the proposal will be needed
		 * later when we send a transaction to the orederer
		 * */
		var proposalResponses = results[0];
		var proposal = results[1];

		/** lets have a look at the responses to see if they are
		all good, if good they will also include signatures
		required to be committed */

		var all_good = true;
		for (var i in proposalResponses) {
			let one_good = false;
			if (proposalResponses && proposalResponses[i].response &&
				proposalResponses[i].response.status === 200) {
				one_good = true;
				logger.info('install proposal was good');
			} else {
				logger.error('install proposal was bad %j',proposalResponses);
			}

			all_good = all_good & one_good;
		}
		if (all_good) {
			logger.info('Successfully sent install Proposal and received ProposalResponse');
		} else {
			error_message = 'Failed to send install Proposal or receive valid response. Response null or status is not 200'
			logger.error(error_message);
		}
	} catch(error) {
		logger.error('Failed to install due to error: ' + error.stack ? error.stack : error);
		error_message = error.toString();
	}

	if (!error_message) {
		let message = util.format('Successfully install chaincode');
		logger.info(message);
		// build a response to send back to the REST caller
		let response = {
			success: true,
			message: message
		};
		return response;
	} else {
		let message = util.format('Failed to install due to:%s',proposalResponses);
		logger.error(message);
		let response = {
			success: false,
			message: message
		};
		return response;
		throw new Error(message);
	}
};


exports.installChaincode = installChaincode;
