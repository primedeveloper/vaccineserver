const Departments = {
    iot: 'org1',
    forecast: 'org2',
    procurement: 'org3',
    distribution: 'org4',
    overall: 'org5'
};

module.exports = Departments;
